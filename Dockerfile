ARG UBU_RELEASE=xenial
FROM ubuntu:${UBU_RELEASE}

RUN apt-get update && apt-get install -yq \
    autoconf \
    automake \
    g++ \
    git \
    gcc \
    make \
    texinfo \
    wget
RUN apt-get clean

ENV BASE_DIR /sources
ENV XEN_ROOT ${BASE_DIR}/xen
ENV TOOLCHAIN_ROOT ${BASE_DIR}/toolchain
ENV MINIOS_ROOT ${BASE_DIR}/minios

ARG XEN_BR=stable-4.12
ARG XEN_REPO=https://delta.lancs.ac.uk/scc/net/unikernel/xen.git
RUN git clone -b ${XEN_BR} ${XEN_REPO} ${XEN_ROOT}

ARG TOOLCHAIN_BR=master
ARG TOOLCHAIN_REPO=https://github.com/sysml/toolchain
RUN git clone -b ${TOOLCHAIN_BR} ${TOOLCHAIN_REPO} ${TOOLCHAIN_ROOT}

ARG MINIOS_BR=master
ARG MINIOS_REPO=https://delta.lancs.ac.uk/scc/net/unikernel/mini-os.git
RUN git clone -b ${MINIOS_BR} ${MINIOS_REPO} ${MINIOS_ROOT}

WORKDIR ${TOOLCHAIN_ROOT}
RUN make -j all && \
    mkdir -p /lib_roots && \
    cp -r ./x86_64-root/* /lib_roots && \
    rm -rf ./*
ENV LWIP_ROOT /lib_roots/x86_64-xen-elf
ENV NEWLIB_ROOT /lib_roots/x86_64-xen-elf

COPY ./build-clickos.sh /scripts/
RUN chmod +x /scripts/build-clickos.sh

ENV OUTPUT_DIR /out
RUN mkdir ${OUTPUT_DIR}

ENV CLICKOS_ROOT ${BASE_DIR}/clickos
ENV CLICKOS_REPO https://delta.lancs.ac.uk/scc/net/unikernel/clickos.git
ENV CLICKOS_BR master

ENV CLICK_STATS_LVL 2

ENTRYPOINT [ "sh", "/scripts/build-clickos.sh" ]