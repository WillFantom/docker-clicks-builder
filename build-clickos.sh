#!/bin/bash
set -e

BOLD="\033[1m"
NORMAL="\033[0m"

# Clones
echo -e "${BOLD}Cloning Repos${NORMAL}"
git clone ${CLICKOS_REPO} -b ${CLICKOS_BR} ${CLICKOS_ROOT}

echo -e "${BOLD}Configuring ClickOS${NORMAL}"
cd ${CLICKOS_ROOT}
./configure --with-xen=${XEN_ROOT} \
            --with-minios=${MINIOS_ROOT} \
            --with-newlib=${NEWLIB_ROOT} \
            --with-lwip=${LWIP_ROOT} \
            --enable-minios \
            --enable-ipsec \
            --enable-local \
            --with-netmap \
            --enable-intel-cpu \
            --enable-smaller-code \
            --enable-nanotimestamp \
            --enable-stats=${CLICK_STATS_LVL}

echo -e "${BOLD}Making Element List${NORMAL}"
make -j elemlist
            
echo -e "${BOLD}Making ClickOS${NORMAL}"
make -j$(getconf _NPROCESSORS_ONLN) minios

echo -e "${BOLD}Moving to OUT DIR${NORMAL}"
cp -R ${CLICKOS_ROOT}/minios/build/* ${OUTPUT_DIR}

echo -e "${BOLD}Done${NORMAL}"
