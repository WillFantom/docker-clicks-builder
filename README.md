# ClickOS Build Script Dockerized

This container is used to build clickos images. Builds with the tag minios-* take the minios source repo as a variable and grabs the minios source after launch, allowing the use of non-stanfdard minios builds. Alternatly, a standard minios is included with any image without minios in the tag. The purpose of this spilt comes from when the toolchain is built.

### How To:
To run, use the following command:
```bash
docker run --rm -t -e CLICK_STATS_LVL 0 \
    -v $(pwd)/out:/out \
    harbor.scc.lancs.ac.uk/unikernels/clickos-build:latest
```
The built images will then be in `./out`

### Notable ENV Vars:

For images with minios within the tag:
 * `MINIOS_REPO` : The git repo containing the MiniOS source
 * `MINIOS_BR` : The branch of the given repo

For both minios tagged images and nonminios tagged images:
 * `CLICKOS_REPO` : The git repo containing the ClickOS source
 * `CLICKOS_BR` : The branch of the given repo
 * `CLICK_STATS_LVL` : The level to which stats are collected in ClickOS [0(none), 1, 2(all)]